from vector import Vector
from projectile import Projectile
from DNA import DNA
import math
from blastParticle import BlastParticle


class Enemy(object):
    def __init__(self, x, y, speedX, speedY, data, name, dna=None,
                 health=2, final=False):
        self.location = Vector(x, y)
        self.speed = Vector(speedX, speedY)
        self.size = 15
        self.maxSpeed = self.speed
        self.maxForce = 4
        if data.level > 1 and dna:
            self.dna = dna
        else:
            self.dna = DNA()
        self.steps = self.dna.genes
        self.windowWidth = data.width
        self.windowHeight = data.height
        self.player = data.player
        self.projectiles = []
        self.moveIndex = 0
        self.moveTime = 1000
        self.shootTime = 10
        self.name = name
        self.color = 'red' if not final else 'orange'
        self.damage = 1 if not final else 4
        # print('genes', self.dna.genes)
        self.time = 0
        self.startAngle = -math.pi / 2
        self.data = data
        self.onScreenTime = 0
        self.health = health
        self.final = final
        self.blastParticles = []

    def getSpeed(self):
        return self.speed

    def addToPos(self, value):
        self.location += value
        if self.moveIndex == len(self.steps):
            self.steps = []

    def goDown(self):
        self.location.y += 10

    def move(self):
        for i in range(5):
            if len(self.steps) != 0:
                if ('shoot' not in self.steps[self.moveIndex %
                                              len(self.steps)].__name__):
                    self.steps[self.moveIndex %
                               len(self.steps)](self, self.player.getLocation())
        if (len(self.steps) != 0 and self.time % self.shootTime == 0 and
                self.steps[self.moveIndex %
                           len(self.steps)].__name__ == 'shootDown'):
            self.steps[self.moveIndex %
                       len(self.steps)](self, self.player.getLocation())
        if (len(self.steps) != 0 and
                self.steps[self.moveIndex %
                           len(self.steps)].__name__ == 'shootPlayer'):
            for i in range(2):
                self.steps[self.moveIndex %
                           len(self.steps)](self, self.player.getLocation())
        self.time += 1
        self.moveIndex += 1

    def shoot(self, destination):
        # Make a projectile along the current vector speed
        x, y = self.location.x, self.location.y
        self.projectiles.append(Projectile(self.location, destination, 'red'))

    def draw(self, canvas):
        if self.data.level < 4:
            self.drawNormal(canvas)
        else:
            self.drawAdvanced(canvas)
        for particle in self.blastParticles:
            particle.draw(canvas)
            particle.move()
            particle.seek()
            if particle.offScreen():
                self.blastParticles.remove(particle)

        self.onScreenTime += 1

    def drawNormal(self, canvas):
        cx, cy = self.location.x, self.location.y
        startAngle = self.startAngle
        topX = cx - math.cos(startAngle) * self.size * 1.5
        topY = cy - math.sin(startAngle) * self.size * 1.5
        rightX = cx + math.cos(startAngle + math.pi / 2) * self.size
        rightY = cy
        bottomX = cx
        bottomY = cy
        leftX = cx - math.cos(startAngle + math.pi / 2) * self.size
        leftY = cy
        canvas.create_polygon(topX, topY, rightX, rightY, bottomX,
                              bottomY, leftX, leftY, fill=self.color)

    def drawAdvanced(self, canvas):
        cx, cy = self.location.x, self.location.y
        startAngle = self.startAngle

        topX = cx - math.cos(startAngle) * self.size * 2
        topY = cy - math.sin(startAngle) * self.size * 2

        rightX = cx + math.cos(startAngle + math.pi / 2) * self.size * 1.3
        rightY = cy

        bottomX = cx
        bottomY = cy + math.sin(startAngle) * 7

        leftX = cx - math.cos(startAngle + math.pi / 2) * self.size * 1.3
        leftY = cy

        canvas.create_polygon(topX, topY, rightX, rightY, bottomX,
                              bottomY, leftX, leftY, fill=self.color)
        canvas.create_line(topX, topY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, rightX, rightY, fill='black')

    def moveProjectiles(self):
        for projectile in self.projectiles:
            projectile.seek()

    def showProjectiles(self, canvas):
        for projectile in self.projectiles:
            projectile.draw(canvas)

    def isPlayerHit(self):
        for projectile in self.projectiles:
            if projectile.collisionWith(self.player):
                self.player.decreaseHealth(self.damage)
                for j in range(20):
                    self.blastParticles.append(BlastParticle(self.player.location, self.windowHeight,
                                                             self.windowWidth, 4,
                                                             Vector(0, self.windowHeight * 2), 'blue'))
                self.projectiles.remove(projectile)

                return [True, self.name]

    def isOffScreen(self):
        x, y = self.location.x, self.location.y
        return x > self.windowWidth or x < 0 or y > self.windowHeight
