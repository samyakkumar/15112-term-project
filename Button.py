class Button(object):
    def __init__(self, location, canvas, data, message):
        self.location = location
        self.canvas = canvas
        self.data = data
        self.message = message

    def showButton(self):
        self.canvas.create_rectangle(self.location.x - 150, self.location.y,
                                     self.location.x + 430,
                                     self.location.y + 80, outline='green')

        self.canvas.create_polygon(self.location.x - 150, self.location.y,
                                   self.location.x - 140,
                                   self.location.y - 20,
                                   self.location.x + 420,
                                   self.location.y - 20,
                                   self.location.x + 430,
                                   self.location.y, fill='green')
        self.canvas.create_text(self.location.x + 160,
                                self.location.y + 30,
                                font=('04b', '19'),
                                text=self.message, fill='green', anchor='n')

    def inButton(self, x, y):
        check = (x > self.location.x - 150 and x < self.location.x + 430
                 and y > self.location.y and y < self.location.y + 80)

        return check
