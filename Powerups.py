from vector import Vector


class PowerUps(object):
    def __init__(self, location, namePowerUp, canvas, data):
        self.allPowerUps = []
        self.allSteps = []
        self.shieldStrength = 10
        self.location = location
        self.namePowerUp = namePowerUp
        self.size = 8
        for powerUp in self.allPowerUps:
            if powerUp.__name__ == self.namePowerUp:
                self.powerUp(canvas, data)

    def missile(self, canvas, data):
        cx, cy = self.location.x, self.location.y
        r = self.size
        canvas.create_oval(x - r, y - r, x + r, y + r, fill='grey')
        # Main rectangel
        canvas.create_rectangle(cx - 5, cy - 10, cx + 5, cy + 10, fill='white')
        # Triangle
        canvas.create_polygon((cx - 5, cy - 10), (cx + 5, cy - 10),
                              (cx, cy - math.sqrt(3) * 10), fill='white')
        # Trapezium
        canvas.create_polygon((cx - 5, cy + 11), (cx + 5, cy + 11),
                              (cx + 7, cy + 13), (cx - 7, cy + 13), fill='white')
        self.move()

    def bullets(self, canvas, data):
        cx, cy = self.location.x, self.location.y
        r = self.size
        canvas.create_oval(x - r, y - r, x + r, y + r, fill='grey')
        # Triangle
        canvas.create_polygon((cx - 5, cy), (cx + 5, cy),
                              (cx, cy - math.sqrt(3) * 10), fill='gold')
        self.move()

    def collideWith(self, other):
        x, y = self.location.x, self.location.y
        otherX, otherY = other.location.x, other.location.y
        return otherX - other.size - 10 < x < otherX + other.size + 10 and otherY - other.size - 10 < y < otherY + other.size + 10

    def move(self):
        self.location + Vector(0, 10)
