
from tkinter import *
import random

def redrawAll(canvas, data):
    xMargin = 50
    yMargin = 50
    width = min ((data.width - 2*xMargin)/data.cells, (data.height - 2*yMargin)/data.cells)
    for row in range(data.cells):
        for col in range(data.cells):
            x = xMargin + col * width
            y = yMargin + row * width

            if (row, col) == data.selectedIndex: fll = "blue"
            elif row == 0 or col == 0 or row == data.cells - 1 or col == data.cells - 1: fll = "green" 
            else: fll = "red"

            canvas.create_rectangle(x, y, x+width, y+width, fill = fll)

    if data.done:
        txt = "Terminated in %d steps" %data.steps
    else: 
        txt = "%d steps" %data.steps
    canvas.create_text(data.width/2, 100, text = txt )

def init(data):
    data.n = 4  
    data.cells = 2 * data.n + 1
    data.selectedIndex = (data.n, data.n)
    data.steps = 0
    data.done = False
    
def toggleImage(data):
    pass

def mousePressed(event, data): pass
    
def keyPressed(event, data):
    if event.keysym == "space":
        if data.done:
            data.done = False
            init(data)
        else:
            data.x, data.y = data.selectedIndex
            data.x += random.choice([-1, 1])
            data.y += random.choice([-1, 1])
            data.selectedIndex = (data.x, data.y)
            data.steps += 1
            if data.selectedIndex[0] == 0 or data.selectedIndex[0] == data.cells - 1 or data.selectedIndex[1] == 0 or data.selectedIndex[1] == data.cells -1:
                data.done = True


def timerFired(data): pass

####################################
# use the run function as-is
####################################

def run(width=300, height=300):
    def redrawAllWrapper(canvas, data):
        canvas.delete(ALL)
        canvas.create_rectangle(0, 0, data.width, data.height,
                                fill='white', width=0)
        redrawAll(canvas, data)
        canvas.update()    

    def mousePressedWrapper(event, canvas, data):
        mousePressed(event, data)
        redrawAllWrapper(canvas, data)

    def keyPressedWrapper(event, canvas, data):
        keyPressed(event, data)
        redrawAllWrapper(canvas, data)

    def timerFiredWrapper(canvas, data):
        timerFired(data)
        redrawAllWrapper(canvas, data)
        # pause, then call timerFired again
        canvas.after(data.timerDelay, timerFiredWrapper, canvas, data)
    # Set up data and call init
    class Struct(object): pass
    data = Struct()
    data.width = width
    data.height = height
    data.timerDelay = 100 # milliseconds
    data.margin = 30

    # create the root and the canvas (Note Change: do this BEFORE calling init!)
    root = Tk()
    init(data)
    canvas = Canvas(root, width=data.width, height=data.height)
    canvas.pack()

    # set up events
    root.bind("<Button-1>", lambda event:
                            mousePressedWrapper(event, canvas, data))
    root.bind("<Key>", lambda event:
                            keyPressedWrapper(event, canvas, data))

    



    timerFiredWrapper(canvas, data)
    # and launch the app
    root.mainloop()  # blocks until window is closed
    print("bye!")

run(700, 700)