from projectile import Projectile
import math

class Player(object):
    def __init__(self, location):
        self.sprite = 'data/player/player.gif'
        self.location = location
        self.size = 15
        self.projectiles = []
        self.health = 80
        self.level = 1
        self.startAngle = -math.pi/2
        self.numEnemiesKilled = 0
        self.expMultiplier = 2
        self.experience = 80
        self.path = []
        self.powerUps = []

    def draw(self, canvas):
        if self.level <= 4:
            self.level1(canvas)
        elif 4 < self.level <= 10:
            self.level2(canvas)
        elif 10<self.level<=14:
            self.level3(canvas)
        elif self.level > 14:
            self.level4(canvas)
        self.path.append(self.location)


        # canvas.create_rectangle(x - size, y - size, x + size, y + size, fill="white")

    def level1(self, canvas):
        cx, cy = self.location.x, self.location.y
        startAngle = self.startAngle

        topX = cx + math.cos(startAngle) * self.size * 2
        topY = cy + math.sin(startAngle) * self.size * 2

        rightX = cx + math.cos(startAngle + math.pi / 2) * self.size
        rightY = cy

        bottomX = cx
        bottomY = cy - math.sin(startAngle) * self.size * 1.5

        leftX = cx - math.cos(startAngle + math.pi / 2) * self.size
        leftY = cy
        canvas.create_polygon(topX, topY, rightX, rightY, bottomX, bottomY, leftX, leftY, fill='white')
        canvas.create_line(topX, topY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, rightX, rightY, fill='black')
        # size = self.size

    def level2(self, canvas):
        cx, cy = self.location.x, self.location.y
        startAngle = self.startAngle

        topX = cx + math.cos(startAngle) * (self.size - 5) * 2
        topY = cy + math.sin(startAngle) * (self.size - 5) * 2

        rightX = cx + math.cos(startAngle + math.pi / 2) * (self.size - 5)
        rightY = cy


        bottomX = cx
        bottomY = cy - math.sin(startAngle) * (self.size - 5) * 1.5

        leftX = cx - math.cos(startAngle + math.pi / 2) * (self.size - 5)
        leftY = cy

        left2X = bottomX - (self.size - 5) * 1.4 - 40
        left2Y = bottomY - 30

        right2X = bottomX + (self.size - 5) * 1.4 + 40
        right2Y = bottomY - 30

        canvas.create_polygon(topX, topY, rightX, rightY, bottomX, bottomY, leftX, leftY, fill='white')
        canvas.create_polygon(rightX, rightY, right2X, right2Y, bottomX, bottomY, fill='white')
        canvas.create_polygon(leftX, leftY, left2X, left2Y, bottomX, bottomY, fill='white')
        canvas.create_line(topX, topY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, rightX, rightY, fill='black')
        canvas.create_line(rightX, rightY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, bottomX, bottomY, fill='black')

    def level3(self, canvas):
        cx, cy = self.location.x, self.location.y
        startAngle = self.startAngle
        topX = cx + math.cos(startAngle) * (self.size ) * 2
        topY = cy + math.sin(startAngle) * (self.size ) * 2
        rightX = cx + math.cos(startAngle + math.pi / 2) * (self.size )
        rightY = cy


        bottomX = cx
        bottomY = cy - math.sin(startAngle) * (self.size - 5) * 1.5
        leftX = cx - math.cos(startAngle + math.pi / 2) * (self.size )
        leftY = cy

        left2X = bottomX - (self.size ) * 1.4 - 40
        left2Y = bottomY - 30

        right2X = bottomX + (self.size ) * 1.4 + 40
        right2Y = bottomY - 30

        left3Y = bottomY + 30
        left3X = bottomX - (bottomX - left2X) / 2
        right3Y = bottomY + 30
        right3X = bottomX + (right2X - bottomX) / 2

        canvas.create_text(leftX, leftY, text='left', fill='white')
        canvas.create_polygon(topX, topY, rightX, rightY, bottomX, bottomY, leftX, leftY, fill='white')
        canvas.create_polygon(rightX, rightY, right2X, right2Y, bottomX, bottomY, fill='white')
        canvas.create_polygon(leftX, leftY, left2X, left2Y, bottomX, bottomY, fill='white')

        canvas.create_polygon(bottomX, bottomY, left3X, left3Y, left2X, left2Y, fill='white')
        canvas.create_polygon(bottomX, bottomY, right3X, right3Y, right2X, right2Y, fill='white')

        canvas.create_line(topX, topY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, rightX, rightY, fill='black')
        canvas.create_line(rightX, rightY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, bottomX, bottomY, fill='black')
        canvas.create_line(bottomX, bottomY, left2X, left2Y, fill='black')
        canvas.create_line(bottomX, bottomY, right2X, right2Y, fill='black')

    def makeProjectile(self, start, dir):
        self.projectiles.append(Projectile(start, dir))

    def drawProjectiles(self, canvas):
        for projectile in self.projectiles:
            projectile.draw(canvas)


    def moveProjectiles(self, data):
        for projectile in self.projectiles:
            projectile.seek()
            if projectile.isOffScreen(data):
                self.projectiles.remove(projectile)


    def move(self, vector):
        self.location += vector

    def getLocation(self):
        return self.location

    def checkHit(self, enemy):
        for projectile in self.projectiles:
            if projectile.collisionWith(enemy):
                self.experience += 1*self.expMultiplier
                self.numEnemiesKilled += 1
                if self.experience % 30 == 0:
                    self.level += 1
                    if self.level > 10:
                        self.level = 10
                return [True, enemy.name]

    def decreaseHealth(self, val):
        self.health -= val

    def getHealth(self):
        return self.health

    def showHealth(self, canvas, data):
        canvas.create_rectangle(0, data.height - 190, 80, data.height + 200, fill='grey')
        canvas.create_text(43, data.height - 150, font=('application', '15', 'bold'), text="Health", fill='white')
        canvas.create_text(43, data.height-130, font=('application', '10', 'bold'), text=self.health, fill='white')

    def showNumKills(self, canvas, data):
        canvas.create_rectangle(0, 0, 80, 100, fill='grey')
        canvas.create_text(40, 40, font=('application', '15', 'bold'), text="Kills", fill='white')
        canvas.create_text(40, 60, font=('application', '10', 'bold'), text=self.numEnemiesKilled, fill='white')

    def showExperience(self, canvas, data):
        canvas.create_rectangle(0, 100, 80, 200, fill='grey')
        canvas.create_text(40, 140, font=('application', '13', 'bold'), text="Exp Gained", fill='white')
        canvas.create_text(40, 180, font=('application', '10', 'bold'), text=self.experience, fill='white')

    def makeTrail(self, canvas):
        initialR = self.size//7
        for i in range(len(self.path) - 1, 0, -1):
            currLoc = self.path[i]
            canvas.create_oval(currLoc.x - initialR*i, currLoc.y - initialR*i + 10,
                               currLoc.x + initialR*i, currLoc.y + initialR*i + 10, fill='orange')
        if len(self.path) > 4:
            self.path.pop(0)