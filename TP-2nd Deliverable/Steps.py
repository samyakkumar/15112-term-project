from vector import Vector


# ADD TIMES FOR EACH FUNCTION

class Steps(object):
    # Each step must be a vector
    def __init__(self):
        self.left = self.left
        self.right = self.right
        self.down = self.down
        self.moveToPlayer = self.moveToPlayer
        self.shootDown = self.shootDown
        self.shootPlayer = self.shootPlayer
        self.steps = []
        self.steps += [self.left] * 10
        self.steps += [self.right] * 10
        self.steps += [self.down] * 40
        self.steps += [self.moveToPlayer] * 10
        self.steps += [self.shootDown] * 6
        self.steps += [self.shootPlayer] * 4

    def getAllSteps(self):
        return self.steps

    # each function returns a direction as per the name of the function
    def left(self, other, playerLoc=None):
        val = Vector(-1, 0)
        other.addToPos(val)

    def right(self, other, playerLoc=None):
        val = Vector(1, 0)
        other.addToPos(val)

    def down(self, other, playerLoc=None):
        val = Vector(0, 1)
        other.addToPos(val)

    # move towards player location
    def moveToPlayer(self, other, playerLoc):
        direction = playerLoc - other.location
        heading = Vector.heading(direction)
        vectorFromHeading = Vector.fromAngle(heading)
        vectorFromHeading *= 3
        other.addToPos(vectorFromHeading)

    def shootDown(self, other, playerLoc=None):
        other.shoot(Vector(other.location.x, other.location.y + 1000))

    def shootPlayer(self, other, playerLoc):
        other.shoot(playerLoc)

