from tkinter import *
import math
from player import Player as player
from enemy import Enemy as enemy
from vector import *
from projectile import *





# MODEL VIEW CONTROLLER (MVC)
####################################
# MODEL:       the data
# VIEW:        redrawAll and its helper functions
# CONTROLLER:  event-handling functions and their helper functions
####################################


####################################
# customize these functions
####################################


# Initialize the data which will be used to draw on the screen.
def init(data):
    # load data as appropriate
    data.player = player(Vector(data.width//2, data.height//2))
    data.background = PhotoImage(file='background.gif').zoom(2, 2)
    print("HI")
    # data.player_img = ImageTk.PhotoImage(data.player_image)

    data.projectiles = []
    data.speed = 10
    data.exploded = []





# These are the CONTROLLERs.
# IMPORTANT: CONTROLLER does *not* draw at all!
# It only modifies data according to the events.
def mousePressed(event, data):
    # use event.x and event.y
    x, y = event.x, event.y
    data.projectiles.append([Projectile(data.player.location, Vector(x, y))])
    print('projectiles', data.projectiles)


def keyPressed(event, data):
    # use event.char and event.keysym
    key = event.keysym
    if key == 'Left' or key == 'a':
        data.player.location += Vector(-data.speed, 0)
    elif key == 'Right' or key == 'd':
        data.player.location += Vector(data.speed, 0)
    elif key == 'Up' or key == 'w':
        data.player.location += Vector(0, -data.speed)
    elif key == 'Down' or key == 's':
        data.player.location += Vector(0, data.speed)


def timerFired(data):
    for projectile in data.projectiles:
        projectile[0].seek()
        reached = projectile[0].reached()
        if reached:
            data.projectiles.remove(projectile)




# This is the VIEW
# IMPORTANT: VIEW does *not* modify data at all!
# It only draws on the canvas.
def redrawAll(canvas, data):
    canvas.create_image(data.width//2, data.height//2, image=data.background)
    # draw in canvas
    x = data.player.location.x
    y = data.player.location.y
    canvas.create_rectangle(x, y, x + 10, y + 10, fill='white')
    for i in range(len(data.projectiles)):
        projectile = data.projectiles[i]
        x, y = projectile[0].location.x, projectile[0].location.y
        if not projectile[0].exploded:
            canvas.create_oval(x, y, x + 15, y + 5, fill=projectile[0].color)



####################################
####################################
# use the run function as-is
####################################
####################################

def run(width=300, height=300):
    def redrawAllWrapper(canvas, data):
        canvas.delete(ALL)
        redrawAll(canvas, data)
        canvas.update()

    def mousePressedWrapper(event, canvas, data):
        mousePressed(event, data)
        redrawAllWrapper(canvas, data)

    def keyPressedWrapper(event, canvas, data):
        keyPressed(event, data)
        redrawAllWrapper(canvas, data)

    def timerFiredWrapper(canvas, data):
        timerFired(data)
        redrawAllWrapper(canvas, data)
        # pause, then call timerFired again
        canvas.after(data.timerDelay, timerFiredWrapper, canvas, data)

    # Set up data and call init
    class Struct(object): pass

    data = Struct()
    data.width = width
    data.height = height
    data.timerDelay = 10  # millisecond
    # create the root and the canvas
    root = Tk()
    canvas = Canvas(root, width=data.width, height=data.height)
    canvas.pack()
    init(data)
    # set up events
    root.bind("<Button-1>", lambda event:
    mousePressedWrapper(event, canvas, data))
    root.bind("<Key>", lambda event:
    keyPressedWrapper(event, canvas, data))
    timerFiredWrapper(canvas, data)
    # and launch the app
    root.mainloop()  # blocks until window is closed
    print("bye!")


run(800, 800)
