import math
class Vector(object):
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return str(self.x) + ' , ' + str(self.y) + ' , ' + str(self.z)

    def __repr__(self):
        return str(self.x) + ' , ' + str(self.y) + ' , ' + str(self.z)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z

    def __add__(self, other):
        x1 = self.x
        y1 = self.y
        z1 = self.z
        x2 = other.x
        y2 = other.y
        z2 = other.z
        return Vector(x1+x2, y1+y2, z1 + z2)

    def __sub__(self, other):
        x1 = self.x
        y1 = self.y
        z1 = self.z
        x2 = other.x
        y2 = other.y
        z2 = other.z
        return Vector((x1 - x2), (y1 - y2), (z1 - z2))

    def __mul__(self, other):
        x1 = self.x
        y1 = self.y
        z1 = self.z
        return Vector(x1*other, y1*other, z1*other)

    def __abs__(self):
        return self.mag()

    def mag(self):
        expression = self.x**2 + self.y**2 + self.z**2
        return math.sqrt(expression)

    def dot(self, other):
        x, y, z = other.x, other.y, other.z
        return self.x * x + self.y * y + self.z * z

    def normalize(self):
        signX = 1 if self.x > 0 else -1
        signY = 1 if self.y > 0 else -1
        signZ = 1 if self.z > 0 else -1
        self.x = 1*signX if self.x != 0 else 0
        self.y = 1*signY if self.y != 0 else 0
        self.z = 1*signZ if self.z != 0 else 0

    def setMag(self, mag):
        self.normalize()
        self.x *= mag
        self.y *= mag
        self.z *= mag

    def cross(self, other):
        x = self.y * other.z - self.z * other.y
        y = self.z * other.x - self.x * other.z
        z = self.x * other.y - self.y * other.x
        return Vector(x, y, z)

    def limit(self, maximum):
        if self.mag() > maximum:
            self.setMag(maximum)

    def dist(self, other):
        x1 = self.x
        y1 = self.y
        z1 = self.z
        x2 = other.x
        y2 = other.y
        z2 = other.z
        expression = (x1 - x2)**2 + (y1 - y2)**2 + (z1 - z2)**2
        return math.sqrt(expression)


