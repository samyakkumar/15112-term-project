from vector import *
import random


class Projectile(object):
    def __init__(self, start, destination, player=True):
        self.player = player
        self.start = start
        self.destination = destination
        self.exploded = False
        self.velocity = Vector(random.uniform(-2, 2), random.uniform(-2, 2))
        self.maxSpeed = 10
        self.location = start
        self.maxForce = 4
        self.color = 'blue' if self.player else 'red'

    def __str__(self):
        return str(self.start) + ',' + str(self.destination)

    def __repr__(self):
        return str(self.start) + ',' + str(self.destination)

    def applyForce(self, steering):
        self.location += steering
        # print('self location', self.location)
        # print('to go to ', self.destination)

    def seek(self):
        desired = self.destination - self.location
        # print('desired', desired)
        desired.setMag(self.maxSpeed)
        steering = desired - self.velocity
        print(steering)
        steering.limit(self.maxForce)
        # print('steering', steering)
        self.applyForce(steering)

    def reached(self):
        if self.location.dist(self.destination) <= self.maxForce:
            self.exploded = True
            return True

    def draw(self, canvas):
        pass
