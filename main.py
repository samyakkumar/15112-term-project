from tkinter import *
import math
from player import Player as player
from enemy import Enemy as enemy
from vector import *
from projectile import *
import random
from population import Population
import time
from DNA import DNA
from Button import Button
from Star import Star


# MODEL VIEW CONTROLLER (MVC)
####################################
# MODEL:       the data
# VIEW:        redrawAll and its helper functions
# CONTROLLER:  event-handling functions and their helper functions
####################################


####################################
# customize these functions
####################################
# Font 04b taken from : http://www.dafont.com/bitmap.php
# From https://www.cs.cmu.edu/~112/notes/notes-strings.html#basicFileIO
def readFile(path):
    with open(path, "rt") as f:
        return f.read()


def writeFile(path, contents):
    with open(path, "wt") as f:
        f.write(contents)


# Initialize the data which will be used to draw on the screen.
def init(data):
    # load data as appropriate
    data.player = player(Vector(data.width // 2, data.height // 2), data)
    data.background = PhotoImage(file='data/background.gif').zoom(2, 2)
    print("Hi")
    data.speed = 15
    data.level = 1
    data.prevLevel = 0
    data.numberOfBots = 2
    data.time = 0
    data.actualTime = 1000
    data.useSeed = False
    data.population = Population(data)
    data.playerAlive = True
    data.screenMode = 'splash'
    data.useSeed = False
    data.arrowKeysHelp = ['Up', 'Down', 'Left', 'Right']
    data.wasdHelp = ['a', 'w', 's', 'd']
    data.mouseClickHelpAttempts = 10
    data.stars = []
    data.numStars = 350
    data.playerCustomBuild = False
    data.playerCustomBuildCoords = []
    data.isPaused = False
    data.showWaitScreen = False
    data.showBoss = False
    data.midLevel = False
    data.win = False
    makeStars(data)


def makeStars(data):
    for i in range(data.numStars):
        startAngle = random.uniform(0, 2 * math.pi)
        currVector = Vector.fromAngle(startAngle) * 3
        data.stars.append(Star(data, currVector))


# These are the CONTROLLERs.
# IMPORTANT: CONTROLLER does *not* draw at all!
# It only modifies data according to the events.

def mousePressed(event, data):
    if data.isPaused:
        return
    x, y = event.x, event.y
    if data.screenMode == 'splash':
        if data.button1.inButton(x, y):
            print('button1')
            data.population.makeNewEnemy(data.numberOfBots)
            data.screenMode = 'game'
        if data.button2.inButton(x, y):
            print('button1')
            data.screenMode = 'help'
        if data.button3.inButton(x, y):
            print('button1')
            data.screenMode = 'builder'
        if data.button4.inButton(x, y):
            print('button1')
            data.screenMode = 'about'
    if data.screenMode == 'game' or data.screenMode == 'help':
        # use event.x and event.y
        data.player.makeProjectile(data.player.location +
                                   Vector(0, -data.player.size),
                                   Vector(x, y))
    if len(data.arrowKeysHelp) == 0 or len(data.wasdHelp) == 0:
        data.mouseClickHelpAttempts -= 1
    if data.screenMode == 'builder':
        data.playerCustomBuildCoords += [(event.x - data.width // 2,
                                          event.y - data.width // 2)]


def keyPressed(event, data):
    # use event.char and event.keysym
    key = event.keysym
    if key == 'p':
        data.isPaused = not data.isPaused
    if data.isPaused:
        return
    if data.screenMode == 'game' or data.screenMode == 'help':
        if key == 'Left' or key == 'a':
            data.player.move(Vector(-data.speed, 0))
        elif key == 'Right' or key == 'd':
            data.player.move(Vector(data.speed, 0))
        elif key == 'Up' or key == 'w':
            data.player.move(Vector(0, -data.speed))
        elif key == 'Down' or key == 's':
            data.player.move(Vector(0, data.speed))
    elif data.screenMode == 'splash' and key == 'g':
        data.population.makeNewEnemy(data.numberOfBots)
        data.screenMode = 'game'
    elif data.screenMode == 'splash' and key == 'h':
        data.screenMode = 'help'
    elif data.screenMode == 'help' and key == 'g':
        data.population.makeNewEnemy(data.numberOfBots)
        data.screenMode = 'game'
    elif data.screenMode == 'splash' and key == 'b':
        data.playerCustomBuild = True
        data.screenMode = 'builder'
    elif data.screenMode == 'splash' and key == 'a':
        data.screenMode = 'about'
    elif data.screenMode == 'splash' and key == 's':
        print('using seed')
        data.useSeed = True
    if data.screenMode == 'help':
        if key in data.arrowKeysHelp:
            data.arrowKeysHelp.remove(key)
        if key in data.wasdHelp:
            data.wasdHelp.remove(key)
    if key == 'e':
        if data.screenMode == 'builder':
            writeFile('customShip.txt', str(data.playerCustomBuildCoords))
            data.player = player(Vector(data.width // 2, data.height // 2),
                                 data, data.playerCustomBuildCoords)
            print('passed')

        data.screenMode = 'splash'


def timerFired(data):
    if data.win:
        return
    if data.isPaused:
        return
    if data.midLevel:
        data.midLevel = not data.midLevel
        data.screenMode = 'game'
        time.sleep(2)
    if data.screenMode == 'game':
        if data.player.getHealth() <= 0:
            data.screenMode = 'gameOver'
        # Move the projectile of the PLAYER
        data.player.moveProjectiles(data)

        # Move the projectiles of the enemy
        data.population.moveProjectiles()

        # Check if the enemies are off screen
        data.population.checkOffScreen()

        # Check if player is hit
        if data.population.checkCollisionsWithPlayer():
            data.playerAlive = False
        # Check if an enemy was hit
        for enemy in data.population.enemies:
            currCheck = data.player.checkHit(enemy)
            if currCheck and currCheck[0]:
                data.population.removeEnemy(currCheck[1])

        # Check if a level is over and make a new generation
        if data.population.checkGameOver():
            # print(data.population.removed)
            data.level += 1
            data.prevLevel += 1
            data.numberOfBots += 1
            data.population.pickParents()
            if data.level == 16:
                data.win = True
                return
            data.population.resetFitnesses()
            data.population.currNameIndex = 0
            if data.level != data.prevLevel:
                data.player.projectiles = []
                data.screenMode = 'midLevel'
                data.midLevel = True
                if not data.showBoss:
                    data.population.makeNewEnemy(data.numberOfBots)
                else:
                    print("MADE BOSS")
                    data.population.makeBoss(data)

    if data.screenMode == 'help':
        # Move the projectile of the PLAYER
        data.player.moveProjectiles(data)

    if data.mouseClickHelpAttempts <= 0:
        init(data)


def showSplashScreen(canvas, data):
    canvas.create_rectangle(0, 0, data.width, data.height, fill='black')
    showStars(canvas, data)
    canvas.create_text(data.width // 2, 80, font=('04b', '25',),
                       text="WeLcOmE To oRiGaMi ShOoTeRs 1!", fill='green')

    if not data.useSeed:
        canvas.create_text(data.width // 2, 120, font=('04b', '16',),
                           text="If YoU WiSh To UsE A SeEd PrEsS s", fill='green')
    else:
        canvas.create_text(data.width // 2, 120, font=('04b', '16',),
                           text="UsInG SeEd", fill='green')

    data.button1 = Button(Vector(data.width // 3, data.height // 5),
                          canvas, data, 'To PlAy PrEeSs g Or ClIcK HeRe')
    data.button1.showButton()

    data.button2 = Button(Vector(data.width // 3, data.height // 5 + 140),
                          canvas, data, 'FoR HeLp PrEeSs h Or ClIcK HeRe')
    data.button2.showButton()

    data.button3 = Button(Vector(data.width // 3, data.height // 5 + 280),
                          canvas, data, 'PrEsS b FoR BuIlDeR Or ClIcK HeRe')
    data.button3.showButton()

    data.button4 = Button(Vector(data.width // 3, data.height // 5 + 420),
                          canvas, data, 'PrEsS a FoR AbOuT Or ClIcK HeRe')
    data.button4.showButton()


def showStars(canvas, data):
    for star in data.stars:
        star.draw(canvas)
        star.move()
    for star in data.stars:
        star.offScreen()


def showGameOver(canvas, data):
    canvas.create_rectangle(0, 0, data.width, data.height, fill='black')
    canvas.create_text(data.width // 2, data.height // 2, font=('04b', '35', 'bold'),
                       text="GaMe OvEr !1!", fill='green')
    data.player.showPlayerDeath(canvas)


def showHelp(canvas, data):
    canvas.create_rectangle(0, 0, data.width, data.height, fill='black')
    canvas.create_text(data.width // 2, data.height // 2 - 300,
                       font=('04b', '35', 'bold'),
                       text="Help :)", fill='green')
    if len(data.arrowKeysHelp) > 0 and len(data.wasdHelp) > 0:
        canvas.create_text(data.width // 2, data.height // 2 - 150,
                           font=('04b', '25', 'bold'),
                           text="--> Use WASD or Arrow keys to Move",
                           fill='green')
    else:
        canvas.create_text(data.width // 2, data.height // 2 - 150,
                           font=('04b', '25', 'bold'),
                           text="--> Click to Shoot!!!", fill='green')
        canvas.create_text(data.width // 2, data.height // 2 - 120,
                           font=('04b', '25', 'bold'),
                           text="--> " + str(data.mouseClickHelpAttempts) +
                           ' before splash screen!',
                           fill='green')


    data.player.draw(canvas)


# This is the VIEW
# IMPORTANT: VIEW does *not* modify data at all!
# It only draws on the canvas.
def redrawAll(canvas, data):
    if data.win:
        showWinScreen(canvas, data)
        return
    if data.screenMode == 'game':
        # Draw the background
        canvas.create_image(data.width // 2, data.height // 2,
                           image=data.background)
        # Show trail
        data.player.makeTrail(canvas)
        # Draw the player
        data.player.draw(canvas)
        # Show blast animations
        data.player.showBlastPoints(canvas)
        # Draw all the projectiles for the PLAYER
        data.player.drawProjectiles(canvas)
        # Show number of kills
        data.player.showNumKills(canvas, data)
        # Show experience gained
        data.player.showExperience(canvas, data)
        # Draw the enemies
        data.population.showEnemies(canvas)
        # Show the player health
        data.player.showHealth(canvas, data)

    elif data.screenMode == 'splash':
        showSplashScreen(canvas, data)
    elif data.screenMode == 'gameOver':
        showGameOver(canvas, data)
    elif data.screenMode == 'help':
        showHelp(canvas, data)
        # Draw all the projectiles for the PLAYER
        data.player.drawProjectiles(canvas)
        # Show the player health
        data.player.showHealth(canvas, data)
        # Show number of kills
        data.player.showNumKills(canvas, data)
        # Show experience gained
        data.player.showExperience(canvas, data)
    elif data.screenMode == 'about':
        showAboutScreen(canvas, data)
    elif data.screenMode == 'builder':
        showBuilder(canvas, data)

    if data.isPaused and not data.showWaitScreen:
        canvas.create_text(data.width // 2, data.height // 2, font=('04b', '25', 'bold'),
                           text='Paused', fill='green')
        showStars(canvas, data)

    if data.level == 14:
        showStars(canvas, data)
        data.showBoss = True

    if data.screenMode == 'midLevel':
        showMid(canvas, data)


def showWinScreen(canvas, data):
    canvas.create_rectangle(0, 0, data.width, data.height, fill='black')
    canvas.create_text(data.width // 2, data.height // 2,
                        font=('04b', '25', 'bold'),
                       text='You Won!!!!', fill='green')
    canvas.create_text(data.width // 2, data.height // 2 + 70,
                        font=('04b', '25', 'bold'),
                       text='Now Close the game!', fill='green')
    showStars(canvas, data)


def showMid(canvas, data):
    canvas.create_rectangle(0, 0, data.width, data.height, fill='black')
    canvas.create_text(data.width // 2, data.height // 2,
                        font=('04b', '25', 'bold'),
                       text='Next Level Loading.....', fill='green')
    showStars(canvas, data)


def showAboutScreen(canvas, data):
    canvas.create_rectangle(0, 0, data.width, data.height, fill='black')
    canvas.create_text(data.width // 2, data.height // 2 - 50,
                            font=('04b', '45', 'bold'),
                       text="Coded with Love ;) ", fill='green', anchor='s')
    canvas.create_text(data.width // 2, data.height // 2 + 60,
                    font=('04b', '45', 'bold'),
                       text="From : Samyak ", fill='green')
    showStars(canvas, data)


def showBuilder(canvas, data):
    canvas.create_rectangle(0, 0, data.width, data.height, fill='black')
    canvas.create_oval(data.width // 2 - 4, data.height // 2 - 4, data.width // 2 + 4,
                       data.height // 2 + 4,
                       fill='white')
    canvas.create_text(data.width // 2, 50, font=('04b', '30', 'bold'),
                       text="Click Anywhre to add points", fill='green')
    canvas.create_text(data.width // 2, 100, font=('04b', '15', 'bold'),
                       text="Draw around the Center Circle", fill='green')
    canvas.create_text(data.width // 2, 150, font=('04b', '15', 'bold'),
                       text="(assuming it is the center of the ship)", fill='green')
    canvas.create_text(data.width // 2, 200, font=('04b', '15', 'bold'),
                       text="When finished press e", fill='green')
    actualDrawing = []
    for point in data.playerCustomBuildCoords:
        actualDrawing += [(point[0] + data.width // 2, point[1] + data.height // 2)]
        canvas.create_oval(point[0] - 2 + data.width // 2, point[1] - 2 + data.height // 2,
                           point[0] + 2 + data.width // 2, point[1] + 2 + data.height // 2,
                           fill='white')
    if len(data.playerCustomBuildCoords) > 0:
        canvas.create_polygon(actualDrawing, fill='white')


# The beautiful run function in main taken from : https://pd43.github.io/notes/notes4-2.html
####################################
####################################
# use the run function as-is
####################################
####################################

def run(width=300, height=300):
    def redrawAllWrapper(canvas, data):
        canvas.delete(ALL)
        redrawAll(canvas, data)
        canvas.update()

    def mousePressedWrapper(event, canvas, data):
        mousePressed(event, data)
        redrawAllWrapper(canvas, data)

    def keyPressedWrapper(event, canvas, data):
        keyPressed(event, data)
        # redrawAllWrapper(canvas, data)

    def timerFiredWrapper(canvas, data):
        timerFired(data)
        redrawAllWrapper(canvas, data)
        # pause, then call timerFired again
        canvas.after(data.timerDelay, timerFiredWrapper, canvas, data)

    # Set up data and call init
    class Struct(object):
        pass

    data = Struct()
    data.width = width
    data.height = height
    data.timerDelay = 10  # millisecond
    # create the root and the canvas
    root = Tk()
    canvas = Canvas(root, width=data.width, height=data.height)
    canvas.pack()
    init(data)
    # set up events
    root.bind("<Button-1>", lambda event:
              mousePressedWrapper(event, canvas, data))
    root.bind("<Key>", lambda event:
              keyPressedWrapper(event, canvas, data))
    timerFiredWrapper(canvas, data)
    # and launch the app
    root.mainloop()  # blocks until window is closed
    print("bye!")


run(800, 800)
