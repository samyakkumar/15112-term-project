from enemy import Enemy
import random
from DNA import DNA
from Boss import Boss

def readFile(path):
    with open(path, "rt") as f:
        return f.read()

def writeFile(path, contents):
    with open(path, "wt") as f:
        f.write(contents)

class Population(object):
    def __init__(self, data):
        self.currNameIndex = 0
        self.enemies = []
        self.enemyNames = [i for i in range(1, 1001)]
        self.fitnesses = {}
        self.data = data
        self.newParents = []
        self.removed = []

    def makeNewEnemy(self, numberBots):
        if self.data.useSeed and self.data.level == 1:
            print('used seed!')
            pickedSeedFile = random.choice(['seeds.txt', 'seeds123.txt',
            'seeds456.txt', 'seeds-best-yet.txt'])
            dna1 = readFile(pickedSeedFile)
            dna2 = readFile(pickedSeedFile)
            enemy1 = Enemy(random.randint(20, self.data.width - 20),
                           random.randint(-5, -1),
                           random.randint(2, 4), random.randint(2, 4),
                           self.data,
                           self.enemyNames[self.currNameIndex], dna1)
            enemy2 = Enemy(random.randint(20, self.data.width - 20),
                           random.randint(-5, -1),
                           random.randint(2, 4),
                           random.randint(2, 4), self.data,
                           self.enemyNames[self.currNameIndex], dna2)
            self.newParents = [enemy1, enemy2]

        if (self.data.level > 1 and self.data.level < 16) or self.data.useSeed:
            print('next level')
            for i in range(numberBots):
                newDna = self.getNewDna(self.newParents)
                self.enemies.append(Enemy(random.randint(20,
                                            self.data.width - 20),
                                          random.randint(-5, -1),
                                          random.randint(2, 4),
                                          random.randint(2, 4), self.data,
                                      self.enemyNames[self.currNameIndex],
                                      newDna))
                if self.enemyNames[self.currNameIndex] not in self.fitnesses:
                    self.fitnesses[self.enemyNames[self.currNameIndex]] = 0
                self.currNameIndex += 1

        elif self.data.level < 16 and not self.data.useSeed:
            for i in range(numberBots):
                self.enemies.append(Enemy(random.randint(20,
                                            self.data.width - 20),
                                          random.randint(-5, -1),
                                          random.randint(2, 4),
                                          random.randint(2, 4), self.data,
                                      self.enemyNames[self.currNameIndex]))
                if self.enemyNames[self.currNameIndex] not in self.fitnesses:
                    self.fitnesses[self.enemyNames[self.currNameIndex]] = 0
                self.currNameIndex += 1


    def showEnemies(self, canvas):
        # Draw all the enemis
        for enemy in self.enemies:
            enemy.draw(canvas)
            # show enemy projectiles
            enemy.showProjectiles(canvas)

    def getNewDna(self, parents):
        parentA = parents[0]
        parentB = parents[1]
        return DNA.crossOver(parentA, parentB)

    def moveProjectiles(self):
        for enemy in self.enemies:
            if enemy.steps != []:
                enemy.move()
            else:
                enemy.goDown()
            enemy.moveProjectiles()

    def checkCollisionsWithPlayer(self):
        for enemy in self.enemies:
            currCheck = enemy.isPlayerHit()
            if currCheck and currCheck[0]:
                if currCheck[1] in self.fitnesses:
                    self.fitnesses[currCheck[1]] += 1000 + enemy.onScreenTime
                else:
                    self.fitnesses[currCheck[1]] = 1000
                return currCheck

    def pickParents(self):
        parentA, parentB = 0, 0
        firstMax = 0
        print('fitness', self.fitnesses)
        for key in self.fitnesses:
            fitness = self.fitnesses[key]
            if fitness >= parentA:
                parentA = key
                firstMax = fitness
        # self.fitnesses[parentA] = 'found parent 1'
        parentA = self.getEnemyFromNames([parentA])
        if firstMax > 1000:
            writeFile('seeds.txt', str(parentA.dna))
        for key in self.fitnesses:
            fitness = self.fitnesses[key]
            if (isinstance(fitness, int) and fitness >= parentB
                        and fitness <= firstMax):
                parentB = key
        parentB = self.getEnemyFromNames([parentB])
        self.newParents = [parentA,parentB]

    def getEnemyFromNames(self, parents):
        for enemy in self.removed:
            if parents[0] == enemy.name:
                return enemy

    def resetFitnesses(self):

        self.fitnesses = {}

    def removeEnemy(self, enemyName):

        for enemy in self.enemies:
            if enemy.name == enemyName:
                enemy.health -= 1
                if enemy.health == 0:
                    self.removed.append(enemy)
                    self.enemies.remove(enemy)

    def checkOffScreen(self):

        for enemy in self.enemies:
            if enemy.isOffScreen():
                self.removeEnemy(enemy.name)

    def checkGameOver(self):

        if self.enemies == []:
            return True
        return False

    def makeBoss(self, data):
        for i in range(5):
            newDna = self.getNewDna(self.newParents)
            self.enemies.append(Enemy(random.randint(20, self.data.width - 20),
                                      random.randint(-5, -1),
                                      random.randint(2, 4),
                                      random.randint(2, 4), self.data,
                                      self.enemyNames[self.currNameIndex],
                                      newDna, 10, True))
