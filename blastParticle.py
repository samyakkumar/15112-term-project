from vector import Vector
import random


class BlastParticle(object):
    def __init__(self, location, height, width, r, desination=None, color=None):
        self.location = location
        self.r = r
        self.velocity = Vector.randomVector()
        self.height = height
        self.width = width
        self.destination = Vector(0, 0) if not desination else desination
        self.maxSpeed = 2 * self.velocity
        self.maxForce = 9
        self.color = 'red' if not color else color

    def draw(self, canvas):
        x, y = self.location.x, self.location.y
        r = self.r
        canvas.create_oval(x - r, y - r, x + r, y + r, fill=self.color)

    def move(self):
        self.location += self.velocity * random.randint(1, 6)
        self.seek()
        self.r -= 0.4

    def offScreen(self):
        return (self.location.x < 0 or self.location.x > self.width or
                self.location.y < 0 or self.location.y > self.height)

    def applyForce(self, force):
        self.velocity += force

    def seek(self):
        disired = self.destination - self.location
        heading = Vector.heading(disired)
        velocity = Vector.fromAngle(heading) * 2
        self.applyForce(velocity)
