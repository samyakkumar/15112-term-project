from projectile import Projectile
from blastParticle import BlastParticle
from Star import Star
from vector import Vector
import random
import math


class Player(object):
    def __init__(self, location, data, customCoords=None):
        self.sprite = 'data/player/player.gif'
        self.location = location
        self.size = 15
        self.projectiles = []
        self.health = 100
        self.level = 1
        self.startAngle = -math.pi / 2
        self.numEnemiesKilled = 0
        self.expMultiplier = 10
        self.experience = 80
        self.path = []
        self.powerUps = []
        self.customCoords = customCoords
        self.windowHeight = data.height
        self.windowWidth = data.width
        self.blastParticles = []
        self.stars = []
        for i in range(300):
            startAngle = random.uniform(0, 2 * math.pi)
            currVector = Vector.fromAngle(startAngle) * 3
            self.stars.append(Star(data, currVector, 'red'))
        self.data = data

    def draw(self, canvas):
        if self.customCoords:
            self.showCustom(self.customCoords, canvas)
        else:
            if self.level <= 4:
                self.level1(canvas)
            elif 4 < self.level <= 10:
                self.level2(canvas)
            elif 10 < self.level <= 14:
                self.level3(canvas)
            elif self.level > 14:
                self.level4(canvas)
        self.path.append(self.location)

    # Taken from : http://www.kosbie.net/cmu/fall-15/15-112/notes/notes-graphics.html
    def rgbString(self, red, green, blue):
        return "#%02x%02x%02x" % (red, green, blue)

    def showCustom(self, corrds, canvas):
        acutalPoints = []
        for point in self.customCoords:
            acutalPoints += [(point[0] + self.location.x,
                              point[1] + self.location.y)]
        canvas.create_polygon(acutalPoints, fill='white')

    def level1(self, canvas):
        cx, cy = self.location.x, self.location.y
        startAngle = self.startAngle

        topX = cx + math.cos(startAngle) * self.size * 2
        topY = cy + math.sin(startAngle) * self.size * 2

        rightX = cx + math.cos(startAngle + math.pi / 2) * self.size
        rightY = cy

        bottomX = cx
        bottomY = cy - math.sin(startAngle) * self.size * 1.5

        leftX = cx - math.cos(startAngle + math.pi / 2) * self.size
        leftY = cy
        canvas.create_polygon(topX, topY, rightX, rightY, bottomX,
                              bottomY, leftX, leftY, fill='white')
        canvas.create_line(topX, topY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, rightX, rightY, fill='black')
        # size = self.size

    def level2(self, canvas):
        cx, cy = self.location.x, self.location.y
        startAngle = self.startAngle

        topX = cx + math.cos(startAngle) * (self.size - 5) * 2
        topY = cy + math.sin(startAngle) * (self.size - 5) * 2

        rightX = cx + math.cos(startAngle + math.pi / 2) * (self.size - 5)
        rightY = cy

        bottomX = cx
        bottomY = cy - math.sin(startAngle) * (self.size - 5) * 1.5

        leftX = cx - math.cos(startAngle + math.pi / 2) * (self.size - 5)
        leftY = cy

        left2X = bottomX - (self.size - 5) * 1.4 - 40
        left2Y = bottomY - 30

        right2X = bottomX + (self.size - 5) * 1.4 + 40
        right2Y = bottomY - 30

        canvas.create_polygon(topX, topY, rightX, rightY, bottomX,
                              bottomY, leftX, leftY, fill='white')
        canvas.create_polygon(rightX, rightY, right2X,
                              right2Y, bottomX, bottomY, fill='white')
        canvas.create_polygon(leftX, leftY, left2X, left2Y,
                              bottomX, bottomY, fill='white')
        canvas.create_line(topX, topY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, rightX, rightY, fill='black')
        canvas.create_line(rightX, rightY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, bottomX, bottomY, fill='black')

    def level3(self, canvas):
        cx, cy = self.location.x, self.location.y
        startAngle = self.startAngle
        topX = cx + math.cos(startAngle) * (self.size) * 2
        topY = cy + math.sin(startAngle) * (self.size) * 2
        rightX = cx + math.cos(startAngle + math.pi / 2) * (self.size)
        rightY = cy

        bottomX = cx
        bottomY = cy - math.sin(startAngle) * (self.size - 5) * 1.5
        leftX = cx - math.cos(startAngle + math.pi / 2) * (self.size)
        leftY = cy

        left2X = bottomX - (self.size) * 1.4 - 40
        left2Y = bottomY - 30

        right2X = bottomX + (self.size) * 1.4 + 40
        right2Y = bottomY - 30

        left3Y = bottomY + 30
        left3X = bottomX - (bottomX - left2X) / 2
        right3Y = bottomY + 30
        right3X = bottomX + (right2X - bottomX) / 2

        canvas.create_text(leftX, leftY, text='left', fill='white')
        canvas.create_polygon(topX, topY, rightX, rightY, bottomX,
                              bottomY, leftX, leftY, fill='white')
        canvas.create_polygon(rightX, rightY, right2X,
                              right2Y, bottomX, bottomY, fill='white')
        canvas.create_polygon(leftX, leftY, left2X,
                              left2Y, bottomX, bottomY, fill='white')

        canvas.create_polygon(bottomX, bottomY, left3X,
                              left3Y, left2X, left2Y, fill='white')
        canvas.create_polygon(bottomX, bottomY, right3X,
                              right3Y, right2X, right2Y, fill='white')

        canvas.create_line(topX, topY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, rightX, rightY, fill='black')
        canvas.create_line(rightX, rightY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, bottomX, bottomY, fill='black')
        canvas.create_line(leftX, leftY, bottomX, bottomY, fill='black')
        canvas.create_line(bottomX, bottomY, left2X, left2Y, fill='black')
        canvas.create_line(bottomX, bottomY, right2X, right2Y, fill='black')

    def makeProjectile(self, start, dir):
        self.projectiles.append(Projectile(start, dir))

    def drawProjectiles(self, canvas):
        for projectile in self.projectiles:
            projectile.draw(canvas)

    def moveProjectiles(self, data):
        for projectile in self.projectiles:
            projectile.seek()
            if projectile.isOffScreen(data):
                self.projectiles.remove(projectile)

    def move(self, vector):
        self.location += vector
        if self.location.x < 0:
            self.location.x = self.windowWidth
        if self.location.x > self.windowWidth:
            self.location.x = 0
        if self.location.y < 0:
            self.location.y = self.windowHeight
        if self.location.y > self.windowHeight:
            self.location.y = 0

    def getLocation(self):
        return self.location

    def checkHit(self, enemy):
        for projectile in self.projectiles:
            if projectile.collisionWith(enemy):
                self.experience += 1 * self.expMultiplier
                self.numEnemiesKilled += 1
                if self.experience % (50 * self.expMultiplier) == 0:
                    self.level += 1
                    if self.level > 10:
                        self.level = 10
                self.projectiles.remove(projectile)
                self.addBlastPoint(enemy.location,
                                   self.windowHeight, self.windowWidth)
                return [True, enemy.name]

    def addBlastPoint(self, location, height, width):
        color = 'orange' if self.data.showBoss else 'red'
        for i in range(0, 30):
            self.blastParticles.append(BlastParticle(location, height,
                                                     width, 6, None))

    def showBlastPoints(self, canvas):
        for blast in self.blastParticles:
            if blast.offScreen():
                self.blastParticles.remove(blast)
        for blast in self.blastParticles:
            blast.draw(canvas)
            blast.move()

    def decreaseHealth(self, val):
        self.health -= val

    def getHealth(self):
        return self.health

    def showHealth(self, canvas, data):
        r = self.translate(int((self.health / 100) * data.width),
                           data.width, 0, 0, 255)
        g = self.translate(int((self.health / 100) * data.width), 0,
                           data.width, 0, 255)
        g = self.translate(int((self.health / 100) * data.width), 0,
                           data.width, 0, 255)
        color = self.rgbString(int(r), int(g), 0)
        canvas.create_rectangle(0, data.height * 0.85,
                                (self.health / 100) * data.width,
                                data.height, fill=color)

        canvas.create_text(data.width // 2 * (self.health / 100),
                           data.height * 0.865,
                           font=('application', '15', 'bold'), text="Health",
                           fill='white')

        # print((0, data.height*0.9, (self.health/100) * data.width, data.height))

        # https://stackoverflow.com/questions/1969240/mapping-a-range-of-values-to-another
    def translate(self, value, leftMin, leftMax, rightMin, rightMax):
        # Figure out how 'wide' each range is
        leftSpan = leftMax - leftMin
        rightSpan = rightMax - rightMin
        # Convert the left range into a 0-1 range (float)
        valueScaled = float(value - leftMin) / float(leftSpan)

        # Convert the 0-1 range into a value in the right range.
        return rightMin + (valueScaled * rightSpan)

    def showNumKills(self, canvas, data):
        canvas.create_rectangle(0, 0, 80, 100, fill='grey')
        canvas.create_rectangle(0, 100 - self.numEnemiesKilled * 0.5,
                                80, 100, fill='red')
        canvas.create_text(40, 40, font=('application', '15', 'bold'),
                           text="Kills", fill='white')
        for i in range(2):
            canvas.create_text(40, 60, font=('application', str(i), 'bold'),
                               text=self.numEnemiesKilled, fill='white')

    def showExperience(self, canvas, data):
        canvas.create_rectangle(0, 100, 80, 200, fill='grey')
        canvas.create_text(40, 140, font=('application', '13', 'bold'),
                           text="Exp Gained", fill='white')
        canvas.create_text(40, 140, font=('application', '13', 'bold'),
                           text="Exp Gained", fill='white')
        canvas.create_text(40, 180, font=('application', '10', 'bold'),
                           text=self.experience, fill='white')

    def showPlayerDeath(self, canvas):

        for star in self.stars:
            star.draw(canvas)
            star.move()
            star.offScreen()

    def makeTrail(self, canvas):
        initialR = self.size // 7
        for i in range(len(self.path) - 1, 0, -1):
            currLoc = self.path[i]
            canvas.create_oval(currLoc.x - initialR * i,
                               currLoc.y - initialR * i + 10,
                               currLoc.x + initialR * i,
                               currLoc.y + initialR * i + 10, fill='orange')
        if len(self.path) > 4:
            self.path.pop(0)
