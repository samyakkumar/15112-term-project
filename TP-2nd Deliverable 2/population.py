from enemy import Enemy
import random
from DNA import DNA

def readFile(path):
    with open(path, "rt") as f:
        return f.read()

def writeFile(path, contents):
    with open(path, "wt") as f:
        f.write(contents)

class Population(object):
    def __init__(self, data):
        self.currNameIndex = 0
        self.enemies = []
        self.enemyNames = [i for i in range(1, 1001)]
        self.fitnesses = {}
        self.data = data
        self.newParents = []
        self.removed = []

    def makeNewEnemy(self, numberBots):
        if self.data.level > 1:
            print('next level')
            for i in range(numberBots):
                newDna = self.getNewDna(self.newParents)
                self.enemies.append(Enemy(random.randint(20, self.data.width - 20),
                                          random.randint(-5, -1), random.randint(2, 4), random.randint(2, 4), self.data,
                                      self.enemyNames[self.currNameIndex], newDna))
                if self.enemyNames[self.currNameIndex] not in self.fitnesses:
                    self.fitnesses[self.enemyNames[self.currNameIndex]] = 0
                self.currNameIndex += 1
        else:
            for i in range(numberBots):
                self.enemies.append(Enemy(random.randint(20, self.data.width - 20),
                                          random.randint(-5, -1), random.randint(2, 4), random.randint(2, 4), self.data,
                                      self.enemyNames[self.currNameIndex]))
                if self.enemyNames[self.currNameIndex] not in self.fitnesses:
                    self.fitnesses[self.enemyNames[self.currNameIndex]] = 0
                self.currNameIndex += 1

    def showEnemies(self, canvas):
        # Draw all the enemis
        for enemy in self.enemies:
            enemy.draw(canvas)
            # show enemy projectiles
            enemy.showProjectiles(canvas)
    def getNewDna(self, parents):
        parentA = parents[0]
        parentB = parents[1]
        return DNA.crossOver(parentA, parentB)
    def moveProjectiles(self):
        for enemy in self.enemies:
            if enemy.steps != []:
                enemy.move()
            else:
                enemy.goDown()
            enemy.moveProjectiles()

    def checkCollisionsWithPlayer(self):
        for enemy in self.enemies:
            currCheck = enemy.isPlayerHit()
            if currCheck and currCheck[0]:
                if currCheck[1] in self.fitnesses:
                    self.fitnesses[currCheck[1]] += 1
                else:
                    self.fitnesses[currCheck[1]] = 1
                return currCheck

    def compareFitnesses(self):
        return

    def pickParents(self):
        parentA, parentB = 0, 0
        firstMax = 0
        print('fitness', self.fitnesses)
        for key in self.fitnesses:
            fitness = self.fitnesses[key]
            if fitness >= parentA:
                parentA = key
                firstMax = fitness
        # self.fitnesses[parentA] = 'found parent 1'
        parentA = self.getEnemyFromNames([parentA])
        if firstMax > 5:
            writeFile('seeds.txt', str(parentA.dna))
        for key in self.fitnesses:
            fitness = self.fitnesses[key]
            if isinstance(fitness, int) and fitness >= parentB and fitness <= firstMax:
                parentB = key
        parentB = self.getEnemyFromNames([parentB])
        self.newParents = [parentA,parentB]
        # print('parentA', parentA)
        # print('parentB', parentB)

    def getEnemyFromNames(self, parents):
        # print('recieved parents', parents)
        for enemy in self.removed:
            # print('element', parents[0])
            # print('enemey', enemy.name)
            if parents[0] == enemy.name:
                return enemy

    def resetFitnesses(self):

        self.fitnesses = {}

    def removeEnemy(self, enemyName):

        for enemy in self.enemies:
            if enemy.name == enemyName:
                enemy.color = 'black'
                self.removed.append(enemy)
                self.enemies.remove(enemy)

    def checkOffScreen(self):

        for enemy in self.enemies:
            if enemy.isOffScreen():
                self.removeEnemy(enemy.name)

    def checkGameOver(self):

        if self.enemies == []:
            return True
        return False
