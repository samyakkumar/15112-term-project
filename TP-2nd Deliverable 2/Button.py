class Button(object):
    def __init__(self, location, canvas, data, message):
        self.location = location
        self.canvas = canvas
        self.data = data
        self.message = message

    def showButton(self):
        self.canvas.create_rectangle(self.location.x - 150, self.location.y, self.location.x + 430,
                                self.location.y + 80, outline='green')

        self.canvas.create_polygon(self.location.x - 150, self.location.y, self.location.x - 140, self.location.y - 20,
                          self.location.x + 420, self.location.y - 20, self.location.x + 430,
                          self.location.y, fill='green')
        self.canvas.create_text(self.location.x + 250, self.location.y + 40, font=('courier new', '25', 'bold'),
                       text=self.message, fill='green', anchor='e')
