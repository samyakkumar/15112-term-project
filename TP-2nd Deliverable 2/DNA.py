import math
from Steps import Steps as steps
import random

# Each step: gene strand is a vector
class DNA(object):
    stepSet = steps()

    def __init__(self, genes=None):
        self.genes = []
        self.geneLength = 100
        if genes:
            self.genes = genes
        else:
            for i in range(self.geneLength):
                currStep = random.choice(self.stepSet.getAllSteps())
                self.genes.append(currStep)

    # Make a new generation from previous generations
    @staticmethod
    def crossOver(parentA, parentB):
        newGenes = []
        # print('got parents', parentA.dna)
        # print('got parents', parentB.dna)
        mid = math.floor(random.randint(0, len(parentA.dna.genes)))
        for i in range(len(parentA.dna.genes)):
            if i > mid:
                newGenes.append(parentA.dna.genes[i])
            else:
                newGenes.append(parentB.dna.genes[i])
        oldGenes = newGenes
        # print("genes before mutation")
        # print(oldGenes)
        newGenes = DNA.mutation(newGenes)
        # print('genes after mutation')
        # print(newGenes)
        return DNA(newGenes)

    @staticmethod
    # For a 20% chance add a random step
    def mutation(newGenes):
        for i in range(len(newGenes)):
            if random.uniform(0, 1) < 0.2:
                randomIndex = random.randint(0, len(newGenes) - 1)
                randomStep = random.choice(DNA.stepSet.steps)

                newGenes[randomIndex] = randomStep
        return newGenes

    def __str__(self):
        return str(self.genes)

