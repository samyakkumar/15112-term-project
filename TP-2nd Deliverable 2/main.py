from tkinter import *
import math
from player import Player as player
from enemy import Enemy as enemy
from vector import *
from projectile import *
import random
from population import Population
import time
from DNA import DNA
from Button import Button


# MODEL VIEW CONTROLLER (MVC)
####################################
# MODEL:       the data
# VIEW:        redrawAll and its helper functions
# CONTROLLER:  event-handling functions and their helper functions
####################################


####################################
# customize these functions
####################################


# Initialize the data which will be used to draw on the screen.
def init(data):
    # load data as appropriate
    data.player = player(Vector(data.width//2, data.height//2))
    # The background is taken from https://opengameart.org/content/complete-spaceship-game-art-pack
    data.background = PhotoImage(file='data/background.gif').zoom(2, 2)
    print("Hi")
    data.speed = 15
    data.level = 1
    data.numberOfBots = 2
    data.time = 0
    data.population = Population(data)
    data.population.makeNewEnemy(data.numberOfBots)
    data.playerAlive = True
    data.screenMode = 'splash'
    data.useSeed = False
    data.arrowKeysHelp = ['Up', 'Down', 'Left', 'Right']
    data.wasdHelp = ['a', 'w', 's', 'd']
    data.mouseClickHelpAttempts = 10






# These are the CONTROLLERs.
# IMPORTANT: CONTROLLER does *not* draw at all!
# It only modifies data according to the events.

def mousePressed(event, data):
    if data.screenMode == 'game' or data.screenMode == 'help':
        # use event.x and event.y
        x, y = event.x, event.y
        data.player.makeProjectile(data.player.location + Vector(0, -data.player.size), Vector(x, y))
    if len(data.arrowKeysHelp) == 0 or len(data.wasdHelp) == 0:
        data.mouseClickHelpAttempts -= 1


def keyPressed(event, data):
    # use event.char and event.keysym
    key = event.keysym
    if data.screenMode == 'game' or data.screenMode =='help':
        if key == 'Left' or key == 'a':
            data.player.move(Vector(-data.speed, 0))
        elif key == 'Right' or key == 'd':
            data.player.move(Vector(data.speed, 0))
        elif key == 'Up' or key == 'w':
            data.player.move(Vector(0, -data.speed))
        elif key == 'Down' or key == 's':
            data.player.move(Vector(0, data.speed))
    elif data.screenMode == 'splash' and key == 'g':
        data.screenMode = 'game'
    elif data.screenMode == 'splash' and key=='h':
        data.screenMode = 'help'
    elif data.screenMode == 'help' and key=='g':
        data.screenMode = 'game'
    if data.screenMode == 'help':
        if key in data.arrowKeysHelp:
            data.arrowKeysHelp.remove(key)
        if key in data.wasdHelp:
            data.wasdHelp.remove(key)


def timerFired(data):
    if data.screenMode == 'game':
        if data.player.getHealth() <= 0:
            data.screenMode = 'gameOver'
        # Move the projectile of the PLAYER
        data.player.moveProjectiles(data)

        # Move the projectiles of the enemy
        data.population.moveProjectiles()

        # Check if the enemies are off screen
        data.population.checkOffScreen()

        # Check if player is hit
        if data.population.checkCollisionsWithPlayer():
            data.playerAlive = False
        # Check if an enemy was hit
        for enemy in data.population.enemies:
            currCheck = data.player.checkHit(enemy)
            if currCheck and currCheck[0]:
                data.population.removeEnemy(currCheck[1])

        # Check if a level is over and make a new generation
        if data.population.checkGameOver():

            print(data.population.removed)
            data.level += 1
            data.numberOfBots += 1
            data.population.pickParents()


            data.population.resetFitnesses()
            data.population.currNameIndex = 0
            data.population.makeNewEnemy(data.numberOfBots)

    if data.screenMode == 'help':
        # Move the projectile of the PLAYER
        data.player.moveProjectiles(data)

    if data.mouseClickHelpAttempts <= 0:
        init(data)





def showSplashScreen(canvas, data):
    canvas.create_rectangle(0, 0, data.width, data.height, fill='black')
    canvas.create_text(data.width//2, 100,font=('courier new', '35', 'bold'),
                       text="WeLcOmE To oRiGaMi ShOoTeRs 1!", fill='green')

    button1 = Button(Vector(data.width//3, data.height//4),canvas,  data, 'To PlAy PrEeSs g')
    button1.showButton()

    button2 = Button(Vector(data.width//3, data.height//4 + 140), canvas, data, 'FoR HeLp PrEeSs h')
    button2.showButton()

    button3 = Button(Vector(data.width//3, data.height//4 + 280), canvas, data, 'PrEsS a FoR AbOuT')
    button3.showButton()


def showGameOver(canvas, data):
    canvas.create_rectangle(0, 0, data.width, data.height, fill='black')
    canvas.create_text(data.width//2, data.height//2,font=('courier new', '45', 'bold'),
                       text="GaMe OvEr !1!", fill='green')

def showHelp(canvas, data):
    canvas.create_rectangle(0, 0, data.width, data.height, fill='black')
    canvas.create_text(data.width//2, data.height//2 - 300,font=('courier new', '45', 'bold'),
                       text="Help :)", fill='green')
    if len(data.arrowKeysHelp) > 0 and len(data.wasdHelp) > 0:
        canvas.create_text(data.width//2, data.height//2 - 150, font=('courier new', '35', 'bold'),
                       text="--> Use WASD or Arrow keys", fill='green')
    else:
        canvas.create_text(data.width//2, data.height//2 - 150, font=('courier new', '35', 'bold'),
                       text="--> Click to Shoot!!!", fill='green')
        canvas.create_text(data.width//2, data.height//2 - 120, font=('courier new', '35', 'bold'),
                       text="--> " + str(data.mouseClickHelpAttempts) + ' before splash screen!', fill='green')

    data.player.draw(canvas)


    


# This is the VIEW
# IMPORTANT: VIEW does *not* modify data at all!
# It only draws on the canvas.
def redrawAll(canvas, data):
    if data.screenMode == 'game':
        # Draw the background
        canvas.create_image(data.width // 2, data.height // 2, image=data.background)
        # Show trail
        data.player.makeTrail(canvas)
        # Draw the player
        data.player.draw(canvas)
        # Draw all the projectiles for the PLAYER
        data.player.drawProjectiles(canvas)
        # Show the player health
        data.player.showHealth(canvas, data)
        # Show number of kills
        data.player.showNumKills(canvas, data)
        # Show experience gained
        data.player.showExperience(canvas, data)
        # Draw the enemies
        data.population.showEnemies(canvas)

    elif data.screenMode == 'splash':
        showSplashScreen(canvas, data)
    elif data.screenMode == 'gameOver':
        showGameOver(canvas, data)
    elif data.screenMode == 'help':
        showHelp(canvas, data)
        # Draw all the projectiles for the PLAYER
        data.player.drawProjectiles(canvas)

    

# The beautiful run function is taken from : https://pd43.github.io/notes/notes4-2.html
####################################
####################################
# use the run function as-is
####################################
####################################

def run(width=300, height=300):
    def redrawAllWrapper(canvas, data):
        canvas.delete(ALL)
        redrawAll(canvas, data)
        canvas.update()

    def mousePressedWrapper(event, canvas, data):
        mousePressed(event, data)
        redrawAllWrapper(canvas, data)

    def keyPressedWrapper(event, canvas, data):
        keyPressed(event, data)
        #redrawAllWrapper(canvas, data)

    def timerFiredWrapper(canvas, data):
        timerFired(data)
        redrawAllWrapper(canvas, data)
        # pause, then call timerFired again
        canvas.after(data.timerDelay, timerFiredWrapper, canvas, data)

    # Set up data and call init
    class Struct(object): pass

    data = Struct()
    data.width = width
    data.height = height
    data.timerDelay = 10  # millisecond
    # create the root and the canvas
    root = Tk()
    canvas = Canvas(root, width=data.width, height=data.height)
    canvas.pack()
    init(data)
    # set up events
    root.bind("<Button-1>", lambda event:
    mousePressedWrapper(event, canvas, data))
    root.bind("<Key>", lambda event:
    keyPressedWrapper(event, canvas, data))
    timerFiredWrapper(canvas, data)
    # and launch the app
    root.mainloop()  # blocks until window is closed
    print("bye!")


run(800, 800)
