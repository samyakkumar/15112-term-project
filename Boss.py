from projectile import Projectile
from enemy import Enemy


class Boss(Enemy):
    def __init__(self, x, y, speedX, speedY, data, name, dna=None):
        super().__init__(x, y, speedX, speedY, data, name, dna=None)

    def draw(self, canvas):
        canvas.create_rectangle(0, 0, self.data.width, 50, fill='red')
